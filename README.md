Ici on a recuperé les ids des class
![id-class](https://gitlab.com/abdoukader/ms3-devoir/-/raw/main/captures/id-class.png?ref_type=heads)

Ici on a recuperé les ids et les noms des class
![id-name-class](https://gitlab.com/abdoukader/ms3-devoir/-/raw/main/captures/id-name-class.png?ref_type=heads)

Ici on a recuperé la liste des etudiants avec leur class
![list-class+students](https://gitlab.com/abdoukader/ms3-devoir/-/raw/main/captures/list-class+students.png?ref_type=heads)

Ici on a recuperé la liste des etudiants avec leur class
![list-classet etudiants](https://gitlab.com/abdoukader/ms3-devoir/-/raw/main/captures/list-classet%20etudiants.png?ref_type=heads)

Ici on a recuperé la liste des etudiants
![list-etudiants](https://gitlab.com/abdoukader/ms3-devoir/-/raw/main/captures/liste-etudiants.png?ref_type=heads)

Ici on a ajouté un etudiant
![ajout-student](https://gitlab.com/abdoukader/ms3-devoir/-/raw/main/captures/ajout-student.png?ref_type=heads)