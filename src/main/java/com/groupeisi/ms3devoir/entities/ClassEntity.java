package com.groupeisi.ms3devoir.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class ClassEntity {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    @OneToMany(mappedBy = "classId")
    private List<StudentEntity> students;

    public ClassEntity(Long id, String name) {
        this.id = id;
        this.name = name;
    }



    /*public ClassEntity(Long id, String name, Collection<StudentEntity> students) {
        this.id = id;
        this.name = name;
        this.students = students;
    }*/
}
