package com.groupeisi.ms3devoir.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentEntity {

    @Id
    @GeneratedValue
    private Long id;
    private String firstName;
    private String lastName;
    private String emailId;
    @ManyToOne(fetch = FetchType.LAZY)
    private ClassEntity classId;


    public StudentEntity( String firstName, String lastName, String emailId, ClassEntity classId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailId = emailId;
        this.classId = classId;
    }

}
