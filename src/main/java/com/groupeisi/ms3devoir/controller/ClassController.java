package com.groupeisi.ms3devoir.controller;

import com.groupeisi.ms3devoir.entities.ClassEntity;
import com.groupeisi.ms3devoir.entities.StudentEntity;
import com.groupeisi.ms3devoir.repositories.ClassRepository;
import com.groupeisi.ms3devoir.repositories.StudentRepository;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.Optional;

@Controller
public class ClassController {
    private final ClassRepository classRepository;
    private final StudentRepository studentRepository;

    public ClassController(ClassRepository classRepository, StudentRepository studentRepository) {
        this.classRepository = classRepository;
        this.studentRepository = studentRepository;
    }

    @QueryMapping
    Iterable<ClassEntity> getClassEntity() {
        return classRepository.findAll();
    }
    @QueryMapping
    Iterable<StudentEntity> getStudentEntity() {
        return studentRepository.findAll();
    }
    @MutationMapping
    Optional<ClassEntity> classById(@Argument Long id) {
        return classRepository.findById(id);
    }

    @MutationMapping
    StudentEntity addStudentEntity(@Argument StudentInput studentInput){
        ClassEntity classEntity = classRepository.findById(studentInput.classId()).orElseThrow(() -> new IllegalArgumentException("class not found"));
        StudentEntity student = new StudentEntity(studentInput.firstname(),studentInput.lastname(), studentInput.emailId(), classEntity );
        return studentRepository.save(student);
    }
    record StudentInput(String lastname, String firstname, String emailId, Long classId){}
}
