package com.groupeisi.ms3devoir;

import com.groupeisi.ms3devoir.entities.ClassEntity;
import com.groupeisi.ms3devoir.entities.StudentEntity;
import com.groupeisi.ms3devoir.repositories.ClassRepository;
import com.groupeisi.ms3devoir.repositories.StudentRepository;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
public class Ms3DevoirApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ms3DevoirApplication.class, args);
    }

    @Bean
    ApplicationRunner applicationRunner(ClassRepository classRepository, StudentRepository studentRepository){
        return args -> {
            ClassEntity m2gl = classRepository.save(new ClassEntity(null,"m2gl isi"));
            ClassEntity diti = classRepository.save(new ClassEntity(null,"diti isi"));
            // Créer les étudiants
            StudentEntity student1 = new StudentEntity("abdou kader","THIAM","aktmere@groupeisi.com",m2gl);
            StudentEntity student2 = new StudentEntity("Yacine","FAYE","yacine@groupeisi.com",diti);
            StudentEntity student3 = new StudentEntity("Nafissatou","DIOP","nafi@groupeisi.com",diti);

            // Lier les étudiants aux classes
            /*student1.setClassEntity(m2gl);
            student2.setClassEntity(m2gl);
            student3.setClassEntity(diti);*/


            // Enregistrer tous les étudiants avec leurs associations de classe
            studentRepository.saveAll(List.of(student1, student2, student3));
        };
    }

}
