package com.groupeisi.ms3devoir.repositories;

import com.groupeisi.ms3devoir.entities.ClassEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface ClassRepository extends JpaRepository<ClassEntity, Long> {

}
